# SMARTER Yocto Build Environment

This repository will help build a Yocto distro
for SMARTER.  Currently the only targets supported
by SMARTER Yocto is the Nvidia Xavier and the Raspberry Pi 4

You can skip the build step, and just download an 
existing built release from gitlab here:
https://gitlab.com/arm-research/smarter/smarter-yocto/-/releases

# Nvidia Jetson Xavier

## Build Instructions

Starting with JetPack 4.2, packages outside the L4T BSP can only be downloaded with an NVIDIA Developer Network login. So to use CUDA 10, cuDNN, and any other packages that require a Devnet login, you must create a Devnet account and download the JetPack packages you need for your builds using NVIDIA SDK Manager which you obtain at: https://developer.nvidia.com/nvidia-sdk-manager.  This version of smarter requires the 4.4 Developer Preview version of the library.

You must then set the variable NVIDIA_SDK_DOWNLOAD to "/path/to/the/downloads" in your environment to make them available to your bitbake builds. This can be the NVIDIA SDK Manager downloads directory, /home/$USER/Downloads/nvidia/sdkm_downloads
If you don't do this setup-environment will fail.

```
% git clone -b dunfell --recurse-submodules https://gitlab.com/arm-research/smarter/smarter-yocto.git
% cd smarter-yocto
% export NVIDIA_SDK_DOWNLOAD=/home/$USER/Downloads/nvidia/sdkm_downloads
% . init-environment
% bitbake core-image-smarter-nvidia
```

On subsequent times, you can just use setup-environment instead of re-invoking init-environment.

## Flashing Instructions

```
(in smarter-yocto)
% mkdir deploy
% cd deploy
% tar xvzf poky/build/tmp/deploy/images/jetson-xavier/core-image-smarter-nvidia-jetson-xavier.tar.gz

# * make sure your Xavier is in recovery mode
#   * Press recovery (middle) button and power or reset button (depending if its off or on)
#   * Release power/reset and then recovery button
# * have your k3s master ip address and token handy

% sudo ./smarter-doflash.sh <k3s-server-ip> <k3s-node-token>
```

# Raspberry Pi 4 (64-bit)

## Build Instructions

```
% git clone -b dunfell --recurse-submodules https://gitlab.com/arm-research/smarter/smarter-yocto.git
% cd smarter-yocto
% . init-environment
% MACHINE="raspberrypi4-64" bitbake core-image-smarter
```

## (Alternate) Download Image

If you are downloding the image instead of building it, untar the package to get the bmap and wic image:
```
% tar xzf core-image-smarter-raspberrypi4-64.tar.gz
# and then follow instructions below but run bmaptool against the unpacked wic versus the built wic
```

## Flashing Instructions

```
# insert sdcard, take note of block device (make sure to umount all mounted partitions)
% bmaptool copy poky/build/tmp/deploy/image/raspberrypi4-64/core-image-smarter-raspberrypi4-64.wic.bz2 /dev/<sdcard-block-device>
% mount /dev/<sdcard-block-device>1 /mnt # Note the 1 here
# edit smarter.cfg - example in https://gitlab.com/arm-research/smarter/meta-smarter/-/raw/dev/recipes-smarter/smarter/files/smarter.cfg
% cp smarter.cfg /mnt
% umount /mnt
```

On subsequent times, you can just use setup-environment instead of re-invoking init-environment.

# Other Resources
* Instructions on setting up SMARTER example: https://gitlab.com/arm-research/smarter/example
* Most details on nvidia installation and options are explained in the meta-tegra wiki [here](https://github.com/madisongh/meta-tegra/wiki)
* Yocto documentation is available [here](https://yoctoproject.org/docs)
