#!/bin/bash
. conf/flash.conf
DEPLOYDIR=`mktemp -d`
CWD=`pwd`
cd $DEPLOYDIR
tar xvzf $CWD/poky/build/tmp/deploy/images/jetson-xavier/core-image-smarter-nvidia-jetson-xavier.tegraflash.tar.gz
./smarter-doflash.sh --k3s-master $K3S_MASTER --k3s-token $K3S_TOKEN $FLASHARGS
cd $CWD
rm -rf $DEPLOYDIR
