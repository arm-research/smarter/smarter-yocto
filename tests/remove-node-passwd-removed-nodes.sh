#!/bin/bash


RANCHER_FILE="/var/lib/rancher/k3s/server/cred/node-passwd"

export KUBECONFIG=/home/ubuntu/edge-kube.config

HOSTS_ON_FILE=$(cut -d "," -f 2 < ${RANCHER_FILE})

HOSTS_ON_SYSTEM=$(kubectl get nodes --no-headers | cut -d " " -f 1)

for i in ${HOSTS_ON_FILE}
do
	OK=$(echo ${HOSTS_ON_SYSTEM} | grep ${i})
	if [ -z ${OK} ]
	then
		echo "${i} node do not exists anymore, removing"
		sed -i -e "/${i}/d" ${RANCHER_FILE}
	else
		echo "${i} node OK"
	fi
done

#echo "host"
#echo ${HOSTS_ON_FILE}
#echo "System"
#echo ${HOSTS_ON_SYSTEM}

echo "From now on just listen to events and clean whenever the node is removed"

while true
do
	kubectl get events -o wide --no-headers --watch-only |
	while read a
	do
		EVENT_OK=$(echo $a | grep "RemovingNode")
		if [ -z "${EVENT_OK}" ]
		then
			echo "NOT Processed: ${a}"
			continue
		fi
		echo "Processed: ${a}"
		NODE=$(echo $a | sed -e "s/^.*node\/\([^ ]*\) .*/\1/")
		if [ -z "${NODE}" ]
		then
			continue
		fi
		echo "Removing Node ${NODE} from the node-passwd file"
		sed -i -e "/${NODE}/d" ${RANCHER_FILE}
	done
done

exit 0
