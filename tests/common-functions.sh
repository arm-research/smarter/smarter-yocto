#!/bin/bash

[[ "$TRACE" ]] && set -ex

function logecho() {
        echo $(date +"%d/%m/%Y %H:%M:%S") $*
}

function check_required_vars() {
        NOK=0
        while [ $# -gt 0 ]
        do
                VARNAME="$1"

                eval RESULT="\$${VARNAME}"

                if [ -z "${RESULT}" ]
                then
                        echo "In order to deploy, ${VARNAME} variable must be set"
                        NOK=1
                fi

                shift 1
        done
}

function check_for_USB_device() {
        USBID=$1
        NOK=0

        EXISTING_USB=$(lsusb 2>/dev/null| grep ${USBID}) &&
        if [ ! -z "${EXISTING_USB}" ]
        then
                return
        fi
        NOK=1
        return
}

function wait_for_USB_device() {
        NOK=0
        USBID=$1
        TIMEOUT=${2:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        SECS_WAIT=5
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                RETURN_CODE=0
                EXISTING_USB=$(lsusb 2>/dev/null| grep ${USBID}) &&
                if [ ! -z "${EXISTING_USB}" ]
                then
                        return
                fi
                sleep ${SECS_WAIT}
        done
        logecho "Device ${USBID} did not appear after ${TIMEOUT}s, giving up"
        NOK=1
}

function is_host_alive() {
        NOK=0
        HOST=$1
        TIMEOUT=${2:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        SECS_WAIT=5
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                RETURN_CODE=0
                PING_OUT=$(ping -c 1 ${HOST} 2>/dev/null) &&
                if [ ! -z "${PING_OUT}" ]
                then
                        return
                fi
                sleep ${SECS_WAIT}
        done
        logecho "Host ${HOST} did not appear after ${TIMEOUT}s, giving up"
        NOK=1
        return
}

function set_ci_context() {
        if [ -z "$CI_COMMIT_TAG" ]; then
                export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
                export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
        else
                export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
                export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
        fi
}

function kube_set_cred() {
        NOK=0
        LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} config set-cluster ci --server=${CI_KUBE_SERVER} 2>&1) ||  {
                NOK=1
                logecho "config set-cluster failed failed log: ${LAST_LOG_RESULT}"
                return
        }
                
        if [ ! -z "${CI_KUBE_CERTIFICATE_AUTHORITY_DATA}" ]
        then
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} config set clusters.ci.certificate-authority-data "${CI_KUBE_CERTIFICATE_AUTHORITY_DATA}" 2>&1) || {
                        NOK=2
                        logecho "config set clusters.ci.certificate-authority-data failed log: ${LAST_LOG_RESULT}"
                        return
                }
        fi
        if [ ! -z "${CI_KUBE_CLIENT_CERTIFICATE_DATA}" ]
        then
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} config set users.myself.client-certificate-data "${CI_KUBE_CLIENT_CERTIFICATE_DATA}" 2>&1) || {
                        NOK=3
                        logecho "config set-credentials myself failed log: ${LAST_LOG_RESULT}"
                        return
                }
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} config set users.myself.client-key-data "${CI_KUBE_CLIENT_KEY_DATA}" 2>&1) || {
                        NOK=3
                        logecho "config set-credentials myself failed log: ${LAST_LOG_RESULT}"
                        return
                }
        elif  [ ! -z  "${CI_KUBE_USERNAME}" ]
        then
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} config set-credentials myself --username=${CI_KUBE_USERNAME} --password=${CI_KUBE_PASSWORD} 2>&1) || {
                        NOK=3
                        logecho "config set-credentials myself failed log: ${LAST_LOG_RESULT}"
                        return
                }
        elif [ ! -z "${CI_KUBE_TOKEN}" ]
        then
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} config set-credentials myself --token=${CI_KUBE_TOKEN} 2>&1) || {
                       NOK=4
                       logecho "config set-credentials myself failed log: ${LAST_LOG_RESULT}"
                       return
                }
        else
                logecho "config set-context CI failed log no authentication provided"
        fi
        LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} config set-context CI --user=myself --cluster=ci 2>&1) || {
                NOK=5
                logecho "config set-context CI failed log: ${LAST_LOG_RESULT}"
                return
        }
        LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} config use-context CI 2>&1) || {
                NOK=6
                logecho "config use-context failed log: ${LAST_LOG_RESULT}"
                return
        }
}

function create_secret() {
        NOK=0
        logecho "Create secret for docker-registry"
        if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
                return
        fi
        if [ -z "${CI_REGISTRY}" ]
        then
                return
        fi
        LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} create secret \
                docker-registry gitlab-registry \
                --docker-server="$CI_REGISTRY" \
                --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
                --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
                --docker-email="$GITLAB_USER_EMAIL" \
                -o yaml --dry-run | ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} replace --force -f - 2>&1) || {
                NOK=1
                logecho "config use-context failed log: ${LAST_LOG_RESULT}"
                return
        }
}

function find_podname_node_prefix() {
        PODNAME_PREFIX=$1
        NODENAME_TO_CHECK=$2
        NAMESPACE_TO_CHECK=$3
        TIMEOUT=${4:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        SECS_WAIT=5
        EXISTING_PODS=""
        NOK=0

        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                RETURN_CODE=0
                EXISTING_PODS=$(
                        ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pods --namespace=${NAMESPACE_TO_CHECK} --no-headers=true \
                                --field-selector="spec.nodeName=${NODENAME_TO_CHECK}" \
                                -o custom-columns=STATUS:.metadata.name | 
                        grep -e "^${PODNAME_PREFIX}"
                ) || RETURN_CODE=$?
                if [ $? -eq 0 ]
                then
                        NUM_PODS=$(echo "${EXISTING_PODS}" | tr " " "\n" | wc -l)
                        if [ ${NUM_PODS} -gt 0 ]
                        then
                                break
                        fi
                else
                        echo "Pod not found error=$?"
                fi
                sleep ${SECS_WAIT}
        done

        PODS_FOUND="${EXISTING_PODS}"
        return
}

function delete_daemonset_if_exists() {
        DS_TO_CHECK=$1
        TIMEOUT=${2:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        NOK=0

        # Check if ds exist`
        logecho "Checking if ds ${DS_TO_CHECK} exists"
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get daemonSet/${DS_TO_CHECK}  --no-headers=true --request-timeout $((${END_SEC}-$(date "+%s")))s 2>/dev/null) || {
                        if [ ! -z "${LAST_LOG_RESULT}" ]
                        then
                                NOK=1
                        fi
                        return
                }
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} delete daemonSet/${DS_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s) || {
                        NOK=2
                        return
                }
        done
        NOK=2
        return 
}
                
function delete_namespace_if_exists() {
        NAMESPACE_TO_CHECK=$1
        TIMEOUT=${2:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        NOK=0

        # Check if ds exist`
        logecho "Checking if namespace ${NAMESPACE_TO_CHECK} exists"
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get namespace/${NAMESPACE_TO_CHECK}  --no-headers=true --request-timeout $((${END_SEC}-$(date "+%s")))s 2>/dev/null) || {
                        if [ ! -z "${LAST_LOG_RESULT}" ]
                        then
                                NOK=1
                        fi
                        return
                }
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} delete namespace/${NAMESPACE_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s) || {
                        NOK=2
                        return
                }
        done
        NOK=2
        return 
}
                
function delete_pod_if_exists() {
        POD_TO_CHECK=$1
        NAMESPACE_TO_CHECK=$2
        TIMEOUT=${3:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        NOK=0

        # Check if pod is running
        logecho "Checking if pod ${POD_TO_CHECK} exists"
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s 2>/dev/null ) || {
                        if [ ! -z "${LAST_LOG_RESULT}" ]
                        then
                               NOK=1
                        fi
                        return
                }
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} delete pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s) || {
                        NOK=1
                        return
                }
        done
        NOK=1
        return 
}
                
function check_if_pod_ended() {
        POD_TO_CHECK=$1
        NAMESPACE_TO_CHECK=$2
        TIMEOUT=${3:-60}
        NOK=0

        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
		WAIT_TIMEOUT=$((${END_SEC}-$(date "+%s")))
		if [ ${WAIT_TIMEOUT} -gt 30 ]
		then
			WAIT_TIMEOUT=30
		fi
                POD_RESULT=$(
                        ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} \
                                -o custom-columns=STATUS:.status.phase --no-headers=true \
                                -w --request-timeout ${WAIT_TIMEOUT}s 2>/dev/null | 
                        while read GET_POD_RESULT
                        do
                                if [ ${GET_POD_RESULT} = "Failed" ]
                                then
                                        echo ${GET_POD_RESULT}
                                        ${PKILLCMD}
                                        exit 2;
                                fi
                                if [ ${GET_POD_RESULT} = "Succeeded" ]
                                then
                                        echo ${GET_POD_RESULT}
                                        ${PKILLCMD}
                                        exit 0;
                                fi
                        done
                ) || {
                        NOK=$?
                        JSON_LOG_OUTPUT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} -o json --request-timeout $((${END_SEC}-$(date "+%s")))s)
                        ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} logs ${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s > ${FULL_LOG_DIR}/${POD_TO_CHECK}.log 2>&1
                        LAST_LOG_RESULT="${POD_RESULT}"
                        return
                }
                if [ ! -z "${POD_RESULT}" ]
                then
                        LAST_LOG_RESULT="${POD_RESULT}"
                        return
                fi
        done
        if [ -z "${POD_RESULT}" ]
        then
                JSON_LOG_OUTPUT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} -o json --request-timeout $((${END_SEC}-$(date "+%s")))s)
                ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} logs ${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s > ${FULL_LOG_DIR}/${POD_TO_CHECK}.log 2>&1
                LAST_LOG_RESULT="TIMEOUT"
                NOK=1
        fi
        return
}


function check_if_pod_running() {
        POD_TO_CHECK=$1
        NAMESPACE_TO_CHECK=$2
        TIMEOUT=${3:-120}

        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        SECS_WAIT=5
        NOK=1
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} --no-headers=true --request-timeout $((${END_SEC}-$(date "+%s")))s 2>&1) && {
                        NOK=0
                        break
                }
                sleep ${SECS_WAIT}
        done
        if [ ${NOK} -gt 0 ]
        then
                return
        fi
        
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
		WAIT_TIMEOUT=$((${END_SEC}-$(date "+%s")))
		if [ ${WAIT_TIMEOUT} -gt 30 ]
		then
			WAIT_TIMEOUT=30
		fi
                POD_RESULT=$(
                        ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} \
                                -o custom-columns=STATUS:.status.phase --no-headers=true \
                                -w --request-timeout ${WAIT_TIMEOUT}s | 
                        while read GET_POD_RESULT
                        do
                                if [ ${GET_POD_RESULT} = "Running" ]
                                then
                                        echo ${GET_POD_RESULT}
                                        ${PKILLCMD}
                                        exit 0;
                                fi
                                if [ ${GET_POD_RESULT} = "Failed" ]
                                then
                                        echo ${GET_POD_RESULT}
                                        ${PKILLCMD}
                                        exit 2;
                                fi
                                if [ ${GET_POD_RESULT} = "Succeeded" ]
                                then
                                        echo ${GET_POD_RESULT}
                                        ${PKILLCMD}
                                        exit 3;
                                fi
                        done
                ) || {
                        NOK=$?
                        JSON_LOG_OUTPUT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} -o json --request-timeout $((${END_SEC}-$(date "+%s")))s)
                        ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} logs ${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s > ${FULL_LOG_DIR}/${POD_TO_CHECK}.log 2>&1
                        LAST_LOG_RESULT="${POD_RESULT}"
                        return
                }
                if [ ! -z "${POD_RESULT}" ]
                then
                        LAST_LOG_RESULT="${POD_RESULT}"
                        return
                fi
        done
        if [ $? -eq 0 -a -z "${POD_RESULT}" ]
        then
		JSON_LOG_OUTPUT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get pod/${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} -o json --request-timeout $((${END_SEC}-$(date "+%s")))s)
                ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} logs ${POD_TO_CHECK} --namespace=${NAMESPACE_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s > ${FULL_LOG_DIR}/${POD_TO_CHECK}.log 2>&1
                LAST_LOG_RESULT="TIMEOUT"
        fi
        return
}

function check_if_node_running() {
        NODENAME_TO_CHECK=$1
        TIMEOUT=${2:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        SECS_WAIT=5

        # If node does not exist the command will return error
        NOK=100
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                NODE_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get node/${NODENAME_TO_CHECK} --no-headers=true 2>&1) && NOK=0 && break
                sleep ${SECS_WAIT}
        done
        if [ ${NOK} -gt 0 ]
        then
                LAST_LOG_RESULT="NODE DOES NOT EXIST"
                return
        fi
        NOK=101
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
              NODE_RESULT=$(
                        ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get node/${NODENAME_TO_CHECK} --no-headers=true \
                        -o 'custom-columns=STATUS:.status.conditions[*].reason' -w \
                        --request-timeout $((${END_SEC}-$(date "+%s")))s 2>&1 |
                        while read GET_NODE_RESULT
                        do
                                if [[ "${GET_NODE_RESULT}" =~ [\ ,]KubeletNotReady ]]
                                then
                                        echo "WithoutCNI"
                                elif [[ "${GET_NODE_RESULT}" =~ [\ ,]NodeStatusUnknown ]]
                                then
                                        echo "NotConnected"
                                elif [[ "${GET_NODE_RESULT}" =~ [\ ,]KubeletReady ]]
                                then
                                        echo "Ready"
                                        ${PKILLCMD}
                                        exit 0
                                        break
                                fi
                        done
                ) && {
                        LAST_LOG_RESULT=$(echo "${NODE_RESULT}" | tail -n 1)
                        if [ "${LAST_LOG_RESULT}" == "Ready" ]
                        then
                                NOK=0
                                return
                        fi
                }
        done

        NOK=1
        LAST_LOG_RESULT=$(echo "${NODE_RESULT}" | tail -n 1)
        if [ -z "${NODE_RESULT}" ]
        then
                LAST_LOG_RESULT="TIMEOUT"
        fi

        return
}


function check_if_node_exists_not_ready() {
        NODENAME_TO_CHECK=$1
        NOK=0

        LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get node/${NODENAME_TO_CHECK} --no-headers=true 2>&1) || NOK=$?

        if [ ${NOK} -gt 0 ]
        then
                return
        fi
        if [[ "${LAST_LOG_RESULT}" =~ [\ ,]Ready ]]  
        then
                NOK=1 # Node exists but Ready
                return
        fi
        # Node exists and it is NotReady
        return
}

function delete_node() {
        NODENAME_TO_CHECK=$1
        NOK=0

        LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} delete node/${NODENAME_TO_CHECK} 2>&1) || NOK=$?
}

function create_object_from_template() {
        NOK=0
        
        LAST_LOG_RESULT=$(
                echo "${1}" | 
                sed -e "s|NAMESPACE|${NAMESPACE}|" \
                    -e "s|NODE_TO_TEST|${NODENAME_TO_TEST}|g" | 
                ${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} apply -f - 2>&1) || NOK=$?
}



function check_if_kata_runtime_class_exists() {
        NOK=0

        LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get runtimeclass --no-headers=true 2>&1) || NOK=$?

        if [ ${NOK} -gt 0 ]
        then
                return
        fi
        if [[ "${LAST_LOG_RESULT}" =~ kata ]]  
        then
                NOK=0 # kata-runtime class exists 
                return
        fi
        # kata-runtime class does not exist
        NOK=1
        return
}


function delete_runtime_class_if_exists() {
        RC_TO_CHECK=$1
        TIMEOUT=${2:-120}
        END_SEC=$(($(date "+%s")+${TIMEOUT}))
        NOK=0

        # Check if ds exist`
        logecho "Checking if runtimeclass ${RC_TO_CHECK} exists"
        while [ $(date "+%s") -lt ${END_SEC} ]
        do
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} get runtimeclass/${RC_TO_CHECK}  --no-headers=true --request-timeout $((${END_SEC}-$(date "+%s")))s 2>/dev/null) || {
                        if [ ! -z "${LAST_LOG_RESULT}" ]
                        then
                                NOK=1
                        fi
                        return
                }
                LAST_LOG_RESULT=$(${KUBECTL_CMD} --kubeconfig ${KUBECTL_CONFIGFILE} delete runtimeclass/${RC_TO_CHECK} --request-timeout $((${END_SEC}-$(date "+%s")))s) || {
                        NOK=2
                        return
                }
        done
        NOK=2
        return 
}
