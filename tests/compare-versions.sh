#!/bin/bash

#set -x

CHKSUMCMD=sha256sum
VERSION_1="$1"
VERSION_2="$2"
TMP_UPDATE_DIR="./updatedir"
CACHE_UPDATE_DIR="./cachedir"
MANIFEST_FILE="${TMP_UPDATE_DIR}/tar/manifest.txt"


if [ $# -ne 2 ]
then
	#"$0: <directory_1> <directory_2>"
	"$0: <version1> <version2>"
	exit 1
fi

if [ ! -d "${CACHE_UPDATE_DIR}" ]
then
	mkdir -p "${CACHE_UPDATE_DIR}"
fi

#if [ -d "${TMP_UPDATE_DIR}" ]
#then
#	rm -rf "${TMP_UPDATE_DIR}"
#fi

#mkdir "${TMP_UPDATE_DIR}"

mkdir -p "${TMP_UPDATE_DIR}/tar/"


function downloadversion() {
	VERSION=$1

	if [ -d "${CACHE_UPDATE_DIR}/files/${VERSION}" ]
	then
		echo "Version ${VERSION} is  already downloaded"
		return
	fi

	mkdir -p "${CACHE_UPDATE_DIR}/files/${VERSION}"

	# TODO: download the files
}

function expandversion() {
	VERSION=$1

	if [ ! -d "${TMP_UPDATE_DIR}/files/${VERSION}" ]
	then
		mkdir -p "${TMP_UPDATE_DIR}/files/${VERSION}"

		CUR_DIR=$(pwd)

		(cd "${TMP_UPDATE_DIR}/files/${VERSION}";tar xvf "${CUR_DIR}/${CACHE_UPDATE_DIR}/files/${VERSION}/"core*.tar.gz;cp "${CUR_DIR}/${CACHE_UPDATE_DIR}/files/"Image* .)
	fi
}

function list_dir_files() {
	VERSION=$1
	if [ ! -d "${TMP_UPDATE_DIR}/files/${VERSION}" ]
	then
		echo "Yocto directory $1 not found"
		exit 1
	fi
	(cd "${TMP_UPDATE_DIR}/files/${VERSION}";find . -type f | sed -e "s/^\.\///" | sort) > "${TMP_UPDATE_DIR}/files/${VERSION}.files_dir.txt"
}

downloadversion "${VERSION_1}"
downloadversion "${VERSION_2}"

expandversion "${VERSION_1}"
expandversion "${VERSION_2}"

list_dir_files "${VERSION_1}"
list_dir_files "${VERSION_2}"

FILESDIFF=$(comm -3 "${TMP_UPDATE_DIR}/files/${VERSION_1}.files_dir.txt" "${TMP_UPDATE_DIR}/files/${VERSION_2}.files_dir.txt")

if [ ! -z "${FILESDIFF}" ]
then
	echo $(echo "${FILESDIFF}" | wc -l)" Files exists only in one of the yoctos.... aborting creation of the difference"
	echo "${FILESDIFF}"
	exit 1
fi


FILESCOMP=$(comm -12 "${TMP_UPDATE_DIR}/files/${VERSION_1}.files_dir.txt" "${TMP_UPDATE_DIR}/files/${VERSION_2}.files_dir.txt" | while read a; do cmp "${TMP_UPDATE_DIR}/files/${VERSION_1}/$a" "${TMP_UPDATE_DIR}/files/${VERSION_2}/$a" 1>/dev/null 2>&1; if [ $? -gt 0 ]; then echo "$a"; fi; done)

OTAPOSSIBLE=true

NFILES_BUP=0
BUP_MAIN_CHANGE=false
BUP_BL_ONLY=false
BUP_XUSB_ONLY=false
BUP_KERNEL_ONLY=false
ROOTFSCHANGED=false

for fileName in $(echo "${FILESCOMP}") 
do
	echo "Testing file $fileName"
	case "$fileName" in
		flash.xml.in)
			#OTAPOSSIBLE=false;;
			;;
		boot.img)
			echo "$fileName ignored, file included in the BUP update";;
		core-image-smarter-nvidia.ext4)
			ROOTFSCHANGED=true;;
		tegra194-p2888-0001-p2822-0000.dtb)
			echo "$fileName ignored, file included in the BUP update";;
		Image-initramfs-jetson-xavier.bup-payload)
			BUP_MAIN_CHANGE=true;;
		Image-initramfs-jetson-xavier.bl_only.bup-payload)
			NFILES_BUP=$((${NFILES_BUP} + 1));
			BUP_FILE_TO_USE="${fileName}"
			BUP_BL_ONLY=true;;
		Image-initramfs-jetson-xavier.kernel_only.bup-payload)
			NFILES_BUP=$((${NFILES_BUP} + 1));
			BUP_FILE_TO_USE="${fileName}"
			BUP_KERNEL_ONLY=true;;
		Image-initramfs-jetson-xavier.xusb_only.bup-payload)
			NFILES_BUP=$((${NFILES_BUP} + 1));
			BUP_FILE_TO_USE="${fileName}"
			BUP_XUSB_ONLY=true;;
		*)
			echo "$fileName modified, dunno how to update that"
			OTAPOSSIBLE=false;;
	esac
done

if [ "${OTAPOSSIBLE}" = false ]
then
	echo "OTA is not possible, reflashing is needed with this changes"
	exit 1
fi

cat << EOF > "${TMP_UPDATE_DIR}/tar/check_update.sh"
#!/bin/sh

echo "Check update"

if [ -e "/etc/SMARTER-VERSION.CHANGED" ]
    echo "Update did not complete, checking if the partial update is this one" 
    cmp /etc/SMARTER-VERSION.CHANGED SMARTER-VERSION.OLD
    if [ \$? -gt 0 ]
    then
	echo "Update did not complete but the partial update is not this one, return NOK"
	exit 1
    fi
else
    cmp /etc/SMARTER-VERSION SMARTER-VERSION.OLD
    if [ \$? -gt 0 ]
    then
        echo "This Update is not for this version, return NOK"
        exit 1
    fi   
fio

echo "THis update is for this version and machine type, continue to update"

exit 0

EOF


cat << EOF > "${TMP_UPDATE_DIR}/tar/start_update.sh"
#!/bin/sh

echo "Begin of update"

cp /etc/SMARTER-VERSION /etc/SMARTER-VERSION.CHANGED 

EOF

if [ "${ROOTFSCHANGED}" = true ]
then
	mkdir -p "${TMP_UPDATE_DIR}/mnt/${VERSION_1}"
	mkdir -p "${TMP_UPDATE_DIR}/mnt/${VERSION_2}"
	mkdir -p "${TMP_UPDATE_DIR}/mnt/updated-files"

	sudo mount -o loop,ro "${TMP_UPDATE_DIR}/files/${VERSION_1}/core-image-smarter-nvidia.ext4" "${TMP_UPDATE_DIR}/mnt/${VERSION_1}"
	sudo mount -o loop,ro "${TMP_UPDATE_DIR}/files/${VERSION_2}/core-image-smarter-nvidia.ext4" "${TMP_UPDATE_DIR}/mnt/${VERSION_2}"
	(cd "${TMP_UPDATE_DIR}/mnt/${VERSION_2}";sudo rsync -vrclHD --delete --dry-run "./" "../${VERSION_1}/" > ../rsync.log 2>&1;
	 head -n -3 ../rsync.log | head -n -3 | tail -n +2 | grep -v "^deleting " > ../files-transfer.log;
	 sudo tar -cvj --no-recursion -f ../../tar/patch -T ../files-transfer.log)
	sudo cp "${TMP_UPDATE_DIR}/mnt/${VERSION_1}/etc/SMARTER-VERSION" "${TMP_UPDATE_DIR}/tar/SMARTER-VERSION.OLD"
	sudo cp "${TMP_UPDATE_DIR}/mnt/${VERSION_2}/etc/SMARTER-VERSION" "${TMP_UPDATE_DIR}/tar/SMARTER-VERSION.NEW"
	sudo umount "${TMP_UPDATE_DIR}/mnt/${VERSION_1}" "${TMP_UPDATE_DIR}/mnt/${VERSION_2}"
	FIRSTRUNMODIFIED=$(grep "^usr/bin/smarter-firstrun.sh" "${TMP_UPDATE_DIR}/mnt/rsync.log")
	head -n -3 "${TMP_UPDATE_DIR}/mnt/rsync.log" | head -n -3 | tail -n +2 | grep "^deleting " | sed -e "s/^deleting //"  > "${TMP_UPDATE_DIR}/tar/deleted_files"
	echo "${FIRSTRUNMODIFIED}"
	if [ ! -z "${FIRSTRUNMODIFIED}" ]
	then
		echo "Unfortunately usr/bin/smarter-firstrun.sh  was changed but it cannot be rerun in OTA, so the update will still be made but it should not be applied before determining that is corret and safe"
	fi
	USER_ID=$(id -u)
	GROUP_ID=$(id -g)
	sudo chown -R ${USER_ID}.${GROUP_ID} "${TMP_UPDATE_DIR}/tar"
	rm "${TMP_UPDATE_DIR}/tar/patch.sh" 
	cat << EOF >> "${TMP_UPDATE_DIR}/tar/start_update.sh"

echo "Update root fs"
mkdir /mnt/update
mount -o bind / /mnt/update
CURDIR=\$(pwd)
(cd /mnt/update;tar xvUf "\${CURDIR}/patch";cat "\${CURDIR}/deleted_files" | while read a
 do
   rm -rf "\${a}"
 done)
umount /mnt/update
EOF

fi

if [ "${BUP_MAIN_CHANGE}" = true ]
then
	if [ -z "${BUP_FILE_TO_USE}" -o ${NFILES_BUP} -gt 1 ]
	then
		# utilize the full BUP file
		BUP_FILE_TO_USE="Image-initramfs-jetson-xavier.bup-payload"
	fi
	cp "${TMP_UPDATE_DIR}/files/${VERSION_2}/${BUP_FILE_TO_USE}" "${TMP_UPDATE_DIR}/tar/bl_update_payload"
	#TODO: copy the BUP_FILE_TO_USE to use and start the install process on the machine

	cat << EOF >> "${TMP_UPDATE_DIR}/tar/start_update.sh"

echo "Update BUP"
rm -rf /opt/ota_package
mkdir -p /opt/ota_package
mv bl_update_payload /opt/ota_package/bl_update_payload
nv_update_engine --install no-reboot
EOF

fi


cat << EOF >> "${TMP_UPDATE_DIR}/tar/start_update.sh"

rm /etc/SMARTER-VERSION.CHANGED
echo "End of update"
exit 0
EOF

rm ${MANIFEST_FILE} 2>/dev/null
(cd ${TMP_UPDATE_DIR}/tar;find . -type f -exec ${CHKSUMCMD} {} \;) > ${MANIFEST_FILE}
(cd ${TMP_UPDATE_DIR}/tar;tar cvzf ../update-${VERSION_1}-${VERSION_2}.tar.bz2 .)

